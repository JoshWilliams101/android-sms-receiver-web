var express = require('express')
  , app            = express()
  , http           = require('http')
  , server         = http.createServer(app)
  , bodyParser     = require('body-parser')
  , path           = require('path')
  , sassMiddleware = require('node-sass-middleware')
  , io             = require('socket.io').listen(server)
  , config         = require('./config')
  , nStore         = require('nstore')
  , flatfile       = require('flat-file-db')
  , db             = flatfile.sync('data/users.db')
  , client         = require('twilio')(config.account_sid, config.auth_token)
  , speakeasy      = require('speakeasy')
  , webClients     = {};

var users;
db.clear(function () {

  //clear database then start again
  users = nStore.new('data/users.db', function () {
    console.log("Loaded users.db");
  });
})


server.listen(config.port);

console.log("Server listening on http:localhost:" + config.port + "!");

//configuration for server
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())


//static folder for bootstrap
app.use('/bower_components', express.static('bower_components'));
app.use('/public', express.static('public'));


//setup global middleware
app.use(function (req, res, next) {
  //attach io object to request object
  req.io = io;
  next();
})


//route for main website
app.get('/',function(req,res){
  res.render('index.jade')
});

//route for message back view
app.get('/message',  verifyUser, function(req,res){
  res.render('index.jade')
});

app.get('/details', function (req, res) {
  res.json(users)
});

app.post('/sendMessage', function (req, res) {

  var rio = req.io;

  console.log("request body is:\n\n\n");
  console.log(req.body);
  console.log("\n\n\n");

  rio.to(req.body.secret).emit('receivedMessage', {
    from: req.body.from,
    message: req.body.message,
    number: req.body.number
  });
  res.end();

})


//route to check if user is verified
function verifyUser(req, res, next) {


  //do some verification here.

  return next();
}

function createUser(phone_number, code, socket, secret) {
  users.save(phone_number, {code: code, verified: false}, function (saverr) {
    if (saverr) {
      throw saverr;
    }
    client.sendSms({
        to: phone_number,
        from: config.twilio_number,
        body: 'Your verification code is: ' + code
    }, function(twilioerr, responseData) {
      if (twilioerr) {
        users.remove(phone_number, function(remerr) {if (remerr) { throw remerr; }});
        socket.emit('update', {message: "Invalid phone number!"});
      } else {
        socket.emit('code_generated');
      }
    });
  });
}

function checkVerified(socket, verified, number) {
  if (verified == true) {
    socket.emit('reset');
    socket.emit('update', {message: "You have already verified " + number + "!"});
    return true;
  }
  return false;
}

//socket io server
io.sockets.on('connection', function(socket) {



  socket.on('register', function(data) {


    //create new object for socket
    webClients[socket.id] = {};



    if(!data.phone_number || !data.secret) {

      socket.emit('update', {message: "Please enter phone number and your secret password"})
      return;
    }


    //phone number and secret is set. set the webClient socket secret
    webClients[socket.id]["secret"] = data.secret;



    var code = speakeasy.totp({secret: config.secret});
    users.get(data.phone_number, function (geterr, doc, key) {
      if (geterr) {
        createUser(data.phone_number, code, socket, data.secret);
      }
      else if (checkVerified(socket, doc.verified, data.phone_number) == false) {
        socket.emit('update', {message: "You have already requested a verification code for that number!"});
        socket.emit('code_generated');
      }
    });
  });

  socket.on('verify', function(data) {
    var code = Math.floor((Math.random()*999999)+111111);
    users.get(data.phone_number, function (geterr, doc, key) {
      if (geterr) {
        socket.emit('reset');
        socket.emit('update', {message: "You have not requested a verification code for " + data.phone_number + " yet!"});
      }
      else if (checkVerified(socket, doc.verified, data.phone_number) == false && doc.code == parseInt(data.code)) {

        console.log("before socket has been verified is: ", webClients[socket.id]["verified"] );

        //save in the socket that its been verified
        webClients[socket.id]["verified"] = true;

        console.log("socket has been verified is: ", webClients[socket.id]["verified"] );

        //emit that code has been verified
        socket.emit('verified');

        //emit update event with successfully verified message
        socket.emit('update', {message: "You have successfully verified " + data.phone_number + "!"});

        //save user in database temporarily until user disconnects
        users.save(data.phone_number, {code: parseInt(data.code), verified: true}, function (saverr) {
          if (saverr) { throw saverr; }

          //set global variable for sockets and attach phone number to the id
          webClients[socket.id]["phone_number"] = data.phone_number;


          //have socket join room based on the phone number
          socket.join(webClients[socket.id]["secret"]);

          console.log("joined room " + webClients[socket.id]["secret"]);


        });
      }
      else {
        socket.emit('update', {message: "Invalid verification code!"});
      }
    });

  });




  socket.on('message_sent', function (data) {

    //make sure socket is verified
    if(webClients[socket.id]["verified"]) {


      client.sendSms({
          to: data.receiver,
          from: config.twilio_number,
          body: 'Send using Twilio API \n\n' + webClients[socket.id]["phone_number"] + ': ' + data.message
      }, function(twilioerr, responseData) {
        if (twilioerr) {
          console.error(twilioerr);
          socket.emit('update', {message: "Something went wrong when sending the message"});
        } else {
          socket.emit('update', {message: "<span class='text-success'> Message sent :) </span>"});
        }
      });


    }
    //
    else {
      socket.emit('update', {message: "This socket has not been verified"})
    }


  });




  //on any socket disconnecting
  socket.on('disconnect', function () {
    if(webClients[socket.id]) {
      //remove the record for the users table based on phone number
      users.remove(webClients[socket.id]["phone_number"], function (err) {
        if (err) { console.error(err); }
        //delete the socket from webClients and the number associated with it.
        delete webClients[socket.id];
      })
    }
  });



});
